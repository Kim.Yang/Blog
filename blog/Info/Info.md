---
title: 关于我
date: 2019-06-01
sticky: 1
---
<Boxx :changeTime='6000'/>
我是 KimYang，一只喜欢仰望星空的程序猿!

   - 🛠  "I now develop Web applications";
   - 🤔  "I focus on GUI and anything about user interface"; 
   - 🔗  "I used C++, JavaScript, Python,Swift, etc";
   - 🦀  "I am currently learning CSPostgraduate and other interesting techniques";
   - 📱  "I am using MBP/Mi Mix3 as develop tools "

<!-- more -->

## 交换友链

想加友链的朋友，可以直接通过 PR 的方式提交信息，合适的基本都加嗷！[传送门在此](https://github.com/KimYangOfCat/Blog/tree/master/blog/.vuepress/config/friends.js)

### 格式

```
地址：https://kimyang.cn(必须)
名称：Kim's Blog(必须)
描述：Kim 的折腾记录站点(可选)
头像：https://kimyang.cn/avatar.jpg(必须)
```